//
//  EditAlertViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/18/21.
//

import UIKit

class EditAlertViewController: UIViewController {

    @IBOutlet var alertView: UIView!
    @IBOutlet var termEditText: UITextField!
    @IBOutlet var definitionEditText: UITextField!
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //card from FlashCardSetDetailViewController
    var card: FlashcardDB = FlashcardDB()
    //use this later to do things to the flashcards potentially
    var parentVC: FlashCardSetDetailViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    func setup()
    {
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
    @IBAction func deleteFlashcard(_ sender: Any) {
        do{
            context.delete(card)
            parentVC?.loaddata()
            parentVC?.tableView.reloadData()
            try context.save()
        }
        catch{
            
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneEditing(_ sender: Any) {
        do{
            
            card.term=termEditText.text
            card.definition = definitionEditText.text
            parentVC?.loaddata()
            parentVC?.tableView.reloadData()
            try context.save()
            
        }
        catch{
        }
        self.dismiss(animated: false, completion: {
            //do something / save the edits
        })
        
    }
    
}
